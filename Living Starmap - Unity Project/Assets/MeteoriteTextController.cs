﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MeteoriteTextController : MonoBehaviour
{

    [SerializeField] string metShowerComming = "Meteor Shower Incoming";
    [SerializeField] string metShowerOn = "Meteor Shower Raining!";
    [SerializeField] float flashingSpeed = 2f;
    TextMeshProUGUI textComponent;
    bool textFlashingUp = true;

    // Start is called before the first frame update
    void Start()
    {
        textComponent = GetComponent<TextMeshProUGUI>();
        HideText();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void changeTextToIncoming()
    {
        textComponent.text = metShowerComming;
        if (textFlashingUp)
        {
            textComponent.alpha += Time.deltaTime * flashingSpeed;
            if (!(textComponent.alpha < 1f))
            {
                textFlashingUp = false;
                textComponent.alpha = 1f;
            }
        }
        else
        {
            textComponent.alpha -= Time.deltaTime * flashingSpeed;
            if (!(textComponent.alpha > 0))
            {
                textFlashingUp = true;
                textComponent.alpha = 0f;
            }
        }


    }
    public void changeTextToOn()
    {
        textComponent.alpha = 1f;
        textComponent.text = metShowerOn;
    }
    public void HideText()
    {
        textComponent.alpha = 0;
    }
}
