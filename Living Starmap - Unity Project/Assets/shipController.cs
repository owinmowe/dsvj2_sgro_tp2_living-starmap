﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipController : MonoBehaviour
{

    [SerializeField] float speed = 25;
    [SerializeField] float rotationSpeed = 1;

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(h, 0, v) * speed;
        transform.position += movement * Time.deltaTime;

        Vector3 lastPosition = transform.position;
        Vector3 wantedPosition = transform.position + movement * Time.deltaTime;
        float angle = RealAngle(lastPosition, wantedPosition); 
        Quaternion currentRotation = transform.rotation;
        Quaternion newRotation = Quaternion.Euler(0, angle, 0);
        Quaternion finalRotation = Quaternion.Slerp(currentRotation, newRotation, Time.deltaTime * rotationSpeed);

        if(Mathf.Abs(h) > 0.001f || Mathf.Abs(v) > 0.001f)
        {
            transform.rotation = newRotation;
        }
    }

    float RealAngle(Vector3 from, Vector3 to)
    {
        Vector3 right = Vector3.right;
        Vector3 direction = to - from;
        float angle = Vector3.Angle(right, direction);
        Vector3 cross = Vector3.Cross(right, direction);
        if(cross.y < 0)
        {
            angle = 360 - angle;
        }
        return angle;
    }

}
