﻿using UnityEngine;
using Cinemachine;

public class Planet : MonoBehaviour
{
    [SerializeField] GameObject virtualCamera;
    [SerializeField] Transform meshTransform;
    public int ID = 0;
    float currentAngle = 0;
    float radius = 0;
    float speed = 0;
    Vector3 rotationDirection = Vector3.one;
    float rotationSpeed = 0;

    public void Init(PlanetData pd)
    {
        ID = pd.id;
        radius = pd.traslationRadius;
        speed = pd.translationSpeed + Random.Range(-pd.randomSpeedVariance, pd.randomSpeedVariance);
        rotationDirection = pd.rotationAxis;
        rotationSpeed = pd.rotationSpeed;
        transform.localScale = Vector3.one * pd.size;
        GetComponentInChildren<MeshRenderer>().material = pd.mat;
        virtualCamera.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = pd.cameraDistance;
        currentAngle = Random.Range(0, 360); //Prewarming planet starting position
    }

    void Update()
    {
        Vector3 v3 = Vector3.zero;
        currentAngle += speed * Time.deltaTime;
        v3.x = radius * Mathf.Cos(currentAngle);
        v3.z = radius * Mathf.Sin(currentAngle);
        transform.localPosition = v3;
        meshTransform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
    }

    public void SetCameraState(bool state)
    {
        virtualCamera.SetActive(state);
    }

}
