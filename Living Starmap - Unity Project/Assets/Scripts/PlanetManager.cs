﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{
    [Header("Ship Related")]
    [SerializeField] GameObject shipCamera;

    [Header("Planet Related")]
    [SerializeField] Planet planetPrefab;
    [SerializeField] List<PlanetData> planetsData;
    private List<Planet> planets = new List<Planet>();

    [Header("Meteorite Related")]
    [SerializeField] MeteoriteTextController metText;
    [SerializeField] Meteorite meteoritePrefab;
    [SerializeField] Transform meteoriteSpawnerTransform;
    [SerializeField] float meteoriteSpawnDistanceFromSun = 2000;
    [SerializeField] float meteorSpawnTimer = 10;
    [SerializeField] int meteoritesAmmount = 10;
    int currentMeteoriteAmmount = 0;
    bool showerActive = false;

    float metTimer;

    private void Start()
    {
        SpawnAllPlanets();
        metTimer = meteorSpawnTimer;
    }

    private void SpawnAllPlanets()
    {
        foreach (PlanetData pd in planetsData)
        {
            planets.Add(SpawnPlanet(pd));
        }
    }

    Planet SpawnPlanet(PlanetData pd)
    {
        Planet p = Instantiate(planetPrefab);
        p.name = pd.planetName;
        p.Init(pd);
        p.transform.SetParent(this.gameObject.transform);
        return p;
    }

    private void Update()
    {
        CheckCameraInput();
        CheckMeteoriteSpawn();
    }

    private void CheckMeteoriteSpawn()
    {
        if(showerActive)
        {
            if (currentMeteoriteAmmount == 0)
            {
                showerActive = false;
                metText.HideText();
            }
        }
        if (metTimer > 0)
        {
            metTimer -= Time.deltaTime;
            if (metTimer < 10)
            {
                metText.changeTextToIncoming();
            }
        }
        else
        {
            showerActive = true;
            metTimer = meteorSpawnTimer;
            metText.changeTextToOn();
            for (int i = 0; i < meteoritesAmmount; i++)
            {
                Vector3 dir = Random.insideUnitCircle.normalized;
                dir.z = dir.y;
                dir.y = 0;
                GameObject metGO = Instantiate<GameObject>(meteoritePrefab.gameObject);
                metGO.transform.parent = meteoriteSpawnerTransform;
                metGO.gameObject.transform.position = dir * meteoriteSpawnDistanceFromSun;
                Meteorite metComponent = metGO.GetComponent<Meteorite>();
                metComponent.SetDirection(-dir);
                metComponent.planetManager = this;
                currentMeteoriteAmmount++;
            }
        }
    }

    public void removeMeteorite()
    {
        currentMeteoriteAmmount--;
    }

    private void CheckCameraInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            ActivateCamera(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ActivateCamera(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ActivateCamera(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ActivateCamera(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ActivateCamera(4);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ActivateCamera(5);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            ActivateCamera(6);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            ActivateCamera(7);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            ActivateCamera(8);
        }
    }

    void ActivateCamera(int id)
    {
        DeactivateAllCameras();
        if(id == 0)
        {
            shipCamera.SetActive(true);
        }
        else
        {
            foreach (Planet planet in planets)
            {
                if (planet.ID == id)
                {
                    planet.SetCameraState(true);
                }
            }
        }
    }

    void DeactivateAllCameras()
    {
        shipCamera.SetActive(false);
        foreach (Planet planet in planets)
        {
            planet.SetCameraState(false);
        }
    }
}
