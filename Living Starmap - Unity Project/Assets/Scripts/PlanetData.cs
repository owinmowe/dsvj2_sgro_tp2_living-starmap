﻿using UnityEngine;

[CreateAssetMenu(fileName = "Planet Data", menuName = "ScriptableObjects/Planet Data", order = 1)]
public class PlanetData : ScriptableObject
{
    [Header("Planet common data")]
    public string planetName;
    [Header("Planet logic data")]
    public float translationSpeed = 10;
    public float randomSpeedVariance = .1f;
    public float traslationRadius;
    public Vector3 rotationAxis;
    public float rotationSpeed = 10;
    public float size = 1;
    public Material mat;
    public int id;
    [Header("Planet camera data")]
    public float cameraDistance = 20f;
}
