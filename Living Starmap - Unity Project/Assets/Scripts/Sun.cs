﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{

    [Header("Sun transform Attributes")]
    [SerializeField] Vector3 rotationDirection = Vector3.zero;
    [SerializeField] float rotationSpeed = 0;

    Light pointLight;
    [Header("Point Light Attributes")]
    [SerializeField] float frequency = 10f;
    [SerializeField] float magnitude = 2f;
    [SerializeField] float minMagnitude = 2f;

    void Start()
    {
        pointLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        pointLight.intensity = minMagnitude + (magnitude + Mathf.Sin(Time.timeSinceLevelLoad * frequency) * magnitude);
        transform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
    }
}
