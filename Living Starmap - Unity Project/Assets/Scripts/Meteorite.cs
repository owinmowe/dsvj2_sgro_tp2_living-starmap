﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteorite : MonoBehaviour
{
    [SerializeField] float minSpeed = 2000;
    [SerializeField] float maxSpeed = 5000;
    [SerializeField] float planetReductionOnImpact = .001f;
    [SerializeField] float safeGuardDespawn = 15;
    public PlanetManager planetManager { set; get; }
    float aux;
    Rigidbody rb;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        aux = safeGuardDespawn;
    }

    private void Update()
    {
        aux -= Time.deltaTime;
        if(aux < 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void SetDirection(Vector3 dir)
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.AddForce(dir * Random.Range(minSpeed, maxSpeed), ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Planet>())
        {
            collision.transform.localScale -= new Vector3(planetReductionOnImpact, planetReductionOnImpact, planetReductionOnImpact);
        }
        planetManager.removeMeteorite();
        Destroy(this.gameObject);
    }

}
